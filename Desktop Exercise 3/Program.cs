﻿using Desktop_Exercise_3.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;

namespace Desktop_Exercise_3
{
  class Program
  {
    static void Main(string[] args)
    {
      var myPeople = new List<Person>();
      var myCustomers = new List<Customer>();
      var myChapmans = new List<Customer>();
      var bigSpenders = new List<Customer>();
      var highPercent = new List<Customer>();
      var bigHeader = new SalesOrderHeader();
      //var highPercent = new List<Customer>();


      //inside this using statement, db is your database connection; the source of all of your queries
      using (var db = new AdventureWorks2014Entities())
      {
        myPeople = db.People.ToList();

        //myCustomers = (from c in db.Customers
        //               join p in db.People on c.PersonID equals p.BusinessEntityID into pde
        //               from p in pde.DefaultIfEmpty()
        //               join s in db.Stores on c.StoreID equals s.BusinessEntityID into sde
        //               from s in sde.DefaultIfEmpty()
        //               join o in db.SalesOrderHeaders on c.CustomerID equals o.CustomerID into ode
        //               from o in ode.DefaultIfEmpty()
        //               orderby p.LastName, p.FirstName, s.Name
        //               select c).Include(x => x.Person).Include(x => x.Store).Include(x => x.SalesOrderHeaders).ToList();

        myCustomers = db.Customers
              .Include(x => x.Person)
              .Include(x => x.Store)
              .Include(x => x.SalesOrderHeaders)
              .OrderBy(x => x.Person.LastName)
              .ThenBy(x => x.Person.FirstName)
              .ToList();

        myChapmans = db.Customers
              .Include(x => x.Person)
              .Include(x => x.Store)
              .Include(x => x.SalesOrderHeaders)
              .Where(x => x.Person.LastName.Equals("Chapman"))
              .ToList();

        bigSpenders = db.Customers
              .Include(x => x.Person)
              .Include(x => x.Store)
              .Include(x => x.SalesOrderHeaders)
              .Where(x => x.SalesOrderHeaders.Sum(y => y.TotalDue) > (decimal)300000)
              .ToList();


        bigHeader = db.SalesOrderHeaders.OrderByDescending(x => x.Freight / x.TotalDue).First();

        highPercent = myCustomers.Where(x => x.CustomerID == bigHeader.CustomerID).ToList();
      }

      //db no longer exists here, garbage collection initiated.
      //path to the output file contained in the project
      var pathTest = @"../../output.txt";
      var q1Path = @"../../query1.txt";
      var q2Path = @"../../query2.txt";
      var q3Path = @"../../query3.txt";
      var q4Path = @"../../query4.txt";
      var q5Path = @"../../query5.txt";
      var q6Path = @"../../query6.txt";

      using (var swt1 = new StreamWriter(pathTest))
      {
        swt1.WriteLine();
        myPeople.ForEach(x => swt1.WriteLine("{0}, {1}", x.LastName, x.FirstName));
      }

      using (var swq1 = new StreamWriter(q1Path))
      {
        swq1.WriteLine("There are {0} accounts.", myCustomers.Count);
        myCustomers.ForEach(x => swq1.WriteLine("  {0}", x.AccountNumber));
      }

      using (var swq2 = new StreamWriter(q2Path))
      {
        swq2.WriteLine("There are {0} accounts.", myCustomers.Count);
        //person name
        swq2.WriteLine("\n*************************************");
        //myPeople.ForEach(p => swq2.WriteLine("    Customer Name: {0}, {1}", p.LastName, p.FirstName));
        foreach (var x in myCustomers)
        {
          if (x.Person != null)
          {
            swq2.WriteLine("Customer ID: {0}\n  Name: {1}, {2}\t  Account Number: {3}", x.CustomerID, x.Person.LastName, x.Person.FirstName, x.AccountNumber);
          }
          else
          {
            swq2.WriteLine("Customer ID: {0}\n  No Name Given\t  Account Number: {1}", x.CustomerID, x.AccountNumber);
          }

          if (x.Store != null)
          {
            swq2.WriteLine("  Store Name: {0}", x.Store.Name);
          }
          else
          {
            swq2.WriteLine("  No Store Association");
          }

          foreach (var s in x.SalesOrderHeaders)
          {
            swq2.WriteLine("      Sub-total: {0:C}\n      (Tax: {1:C}, Shipping: {2:C})\n    Total Due: {3:C}", s.SubTotal, s.TaxAmt, s.Freight, s.TotalDue);
          }
          swq2.WriteLine("\n*************************************");
        }


        using (var swq3 = new StreamWriter(q3Path))
        {
          swq3.WriteLine("There are {0} customers with the last name Chapman.", myChapmans.Count);
          //person name
          swq3.WriteLine("*************************************");
          //myPeople.ForEach(p => swq2.WriteLine("    Customer Name: {0}, {1}", p.LastName, p.FirstName));
          foreach (var x in myChapmans)
          {

            swq3.WriteLine("Customer ID: {0}\n  Name: {1}, {2}\t  Account Number: {3}", x.CustomerID, x.Person.LastName, x.Person.FirstName, x.AccountNumber);


            if (x.Store != null)
            {
              swq3.WriteLine("  Store Name: {0}", x.Store.Name);
            }
            else
            {
              swq3.WriteLine("  No Store Association");
            }

            foreach (var s in x.SalesOrderHeaders)
            {
              swq3.WriteLine("      Sub-total: {0:C}\n      (Tax: {1:C}, Shipping: {2:C})\n    Total Due: {3:C}\n", s.SubTotal, s.TaxAmt, s.Freight, s.TotalDue);
            }
            swq3.WriteLine("*************************************");
          }

          using (var swq4 = new StreamWriter(q4Path))
          {
            swq4.WriteLine("There are {0} accounts with total sales over $300,000.\n", bigSpenders.Count);
            //person name
            swq4.WriteLine("*************************************");
            //myPeople.ForEach(p => swq2.WriteLine("    Customer Name: {0}, {1}", p.LastName, p.FirstName));
            foreach (var x in bigSpenders)
            {
              swq4.WriteLine("Customer ID: {0}\n  Name: {1}, {2}\t  Account Number: {3}", x.CustomerID, x.Person.LastName, x.Person.FirstName, x.AccountNumber);

              if (x.Store != null)
              {
                swq4.WriteLine("  Store Name: {0}", x.Store.Name);
              }
              else
              {
                swq4.WriteLine("  No Store Association");
              }

              decimal numberOfTotals = 0;
              foreach (var s in x.SalesOrderHeaders)
              {
                numberOfTotals = x.SalesOrderHeaders.Sum(t => t.TotalDue);

                swq4.WriteLine("      Sub-total: {0:C}\n      (Tax: {1:C}, Shipping: {2:C})\n    Total Due: {3:C}", s.SubTotal, s.TaxAmt, s.Freight, s.TotalDue, numberOfTotals);
              }
              swq4.WriteLine("  Total Customer Sales: {0:C}\n", numberOfTotals);
              swq4.WriteLine("*************************************");
            }

            using (var swq5 = new StreamWriter(q5Path))
            {

              swq5.WriteLine("There are {0} accounts.", myCustomers.Count);
              swq5.WriteLine("*************************************");
              foreach (var x in myCustomers)
              {
                if (x.Person != null)
                {
                  swq5.WriteLine("Customer ID: {0}\n  Name: {1}, {2}\t  Account Number: {3}", x.CustomerID, x.Person.LastName, x.Person.FirstName, x.AccountNumber);
                }
                else
                {
                  swq5.WriteLine("Customer ID: {0}\n  No Name Given\t  Account Number: {1}", x.CustomerID, x.AccountNumber);
                }

                if (x.Store != null)
                {
                  swq5.WriteLine("  Store Name: {0}", x.Store.Name);
                }
                else
                {
                  swq5.WriteLine("  No Store Association");
                }

                decimal percentage = 0;
                decimal numberOfTotals = 0;
                foreach (var s in x.SalesOrderHeaders)
                {
                  numberOfTotals = x.SalesOrderHeaders.Sum(t => t.TotalDue);
                  percentage = (s.Freight / s.TotalDue) * 100;

                  swq5.WriteLine("      Sub-total: {0:C}\n     (Tax: {1:C}, Shipping: {2:C}) Freight Percentage: {3:F2}%\n    Total Due: {4:C}", s.SubTotal, s.TaxAmt, s.Freight, percentage, s.TotalDue);
                }
                swq5.WriteLine("  Total Customer Sales: {0:C}\n", numberOfTotals);
                swq5.WriteLine("*************************************");
              }

              using (var swq6 = new StreamWriter(q6Path))
              {
                swq6.WriteLine("Highest Freight Percentage Paid\n");
                foreach (var x in highPercent)
                {
                  if (x.Person != null)
                  {
                    swq6.WriteLine("Customer ID: {0}\n  Name: {1}, {2}\t  Account Number: {3}", x.CustomerID, x.Person.LastName, x.Person.FirstName, x.AccountNumber);
                  }
                  else
                  {
                    swq6.WriteLine("Customer ID: {0}\n  No Name Given\t  Account Number: {1}", x.CustomerID, x.AccountNumber);
                  }

                  if (x.Store != null)
                  {
                    swq6.WriteLine("  Store Name: {0}", x.Store.Name);
                  }
                  else
                  {
                    swq6.WriteLine("  No Store Association");
                  }

                  decimal percentage = 0;
                  decimal numberOfTotals = 0;
                  foreach (var s in x.SalesOrderHeaders)
                  {
                    numberOfTotals = x.SalesOrderHeaders.Sum(t => t.TotalDue);
                    percentage = (s.Freight / s.TotalDue) * 100;

                    swq6.WriteLine("      Sub-total: {0:C}\n      (Tax: {1:C}, Shipping: {2:C}) Freight Percentage: {3:F2}%\n    Total Due: {4:C}", s.SubTotal, s.TaxAmt, s.Freight, percentage, s.TotalDue);
                  }
                  swq6.WriteLine("\nTotal Customer Sales: {0:C}\n", numberOfTotals);
                }
              }
              }
            }
          }
        }
      }
    }
  }